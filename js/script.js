let number = null;
while (!isNumber(number)) {
    number = prompt("Введіть число");
}

for (let k = 0; k <= number; k++) {

    if (number < 5) {
        console.log("Sorry, no numbers");
    }

    if (k % 5 === 0) {
        console.log(k);
    }
}

function isNumber(number) {
    return number !== null && number !== "" && !isNaN(number) && Number.isInteger(+number);
}

let m = null
let n = null
while (true) {
    m = prompt("Введіть число m")
    n = prompt("Введіть число n")

    if (!isNumber(m) || !isNumber(n) || m > n) {
        console.log("Введіть коректні дані, це мають бути числа та  m < n")
        continue;
    }


    for (let i = m; i <= +n; i++) {
        let isPrime = true;

        for (let j = 2; j < i; j++) {
            if (i % j === 0) {
                isPrime = false;
                break;
            }
        }

        if (isPrime) {
            console.log(i);
        }
    }

    break;

}